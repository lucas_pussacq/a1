package com.cs442spring13.todolist;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class TODOList extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.todolist);

		// Get references to UI widgets
		ListView myListView = (ListView)findViewById(R.id.todoList);
		final EditText myEditText = (EditText)findViewById(R.id.inputTODO);

		// Create the Array List of to do items
		final ArrayList<String> todoItems = new ArrayList<String>();

		// Create the Array Adapter to bind the array to the List View
		final ArrayAdapter<String> arrayAdapter;

		arrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1,
				todoItems);

		// Bind the Array Adapter to the List View
		myListView.setAdapter(arrayAdapter);

		myEditText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) { 
				if (event.getAction() == KeyEvent.ACTION_DOWN)
					if ((keyCode == KeyEvent.KEYCODE_DPAD_CENTER) ||
							(keyCode == KeyEvent.KEYCODE_ENTER)) {
						if(!myEditText.getText().toString().replaceAll(" ||\t", "").isEmpty()){
							todoItems.add(0, myEditText.getText().toString());
							arrayAdapter.notifyDataSetChanged();
							myEditText.setText("");
						}
						return true;
					}
				return false;
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.todolist, menu);
		return true;
	}

}
