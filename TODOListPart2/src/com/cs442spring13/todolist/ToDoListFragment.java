package com.cs442spring13.todolist;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ToDoListFragment extends Fragment {

	ListView _listView;
	Activity _activity;
	ArrayAdapter<String> _arrayAdapter;
	List<String> _todoItem;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.todolistview, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		_todoItem = new ArrayList<String>();
		_listView = (ListView) _activity.findViewById(R.id.listView);
		
		_arrayAdapter = new ArrayAdapter<String>(_activity, R.layout.todoitem, _todoItem);
		_listView.setAdapter(_arrayAdapter);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this._activity = activity;
	}

	public void add(String value) {
		_todoItem.add(0, value);
		_arrayAdapter.notifyDataSetChanged();		
	}

}
