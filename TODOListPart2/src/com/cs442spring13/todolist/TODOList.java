package com.cs442spring13.todolist;

import java.util.ArrayList;
import java.util.List;

import android.R.array;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

public class TODOList extends Activity {

	public EditText myEditText;
	public ToDoListFragment todoListFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.todolist);
		
		myEditText = (EditText)findViewById(R.id.inputTODO);
		
	    FragmentManager fm = getFragmentManager();
	    todoListFragment = (ToDoListFragment)fm.findFragmentById(R.id.todoListFragment);

		myEditText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) { 
				if (event.getAction() == KeyEvent.ACTION_DOWN)
					if ((keyCode == KeyEvent.KEYCODE_DPAD_CENTER) ||
							(keyCode == KeyEvent.KEYCODE_ENTER)) {
						if(!myEditText.getText().toString().trim().isEmpty()){
							todoListFragment.add(myEditText.getText().toString().trim());
							myEditText.setText("");
						}
						return true;
					}
				return false;
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.todolist, menu);
		return true;
	}

}
